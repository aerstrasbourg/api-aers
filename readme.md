## Epitech TV - API AER

### Pré-requis

* PHP >= 5.5.9
* Pré-requis Lumen : http://lumen.laravel.com/docs/installation

---
### Installation

```
git clone git@bitbucket.org:aerstrasbourg/api-aers.git
cd api-aers
cp .env.example .env
nano .env
```

* Passer `APP_DEBUG` à `false`
* Renseigner le login epitech pour `INTRA_USER`
* Renseigner le mot de passe unix en base64 pour `INTRA_PASSWORD`

```
composer update // Téléchargement des modules de Laravel
chmod 0777 -R storage
```

---
### Utilisation

L'API permet d'utiliser 4 routes différentes :

* `GET /permanence` : Retourne la liste des logins des AERs de garde pour la journée. Exemple de données renvoyées :
```
[
	"buchse_a",
	"vermot_r"
]
```
* `GET /permanence/{login}` : Renvoie le status (de garde ou non) de l'AER `login`. Exemple de données renvoyées :
```
[
	true
]
```
* `POST /status` : Récupère le status (`at_school`, `at_home` ou `offline`) des personnes dont le login est passé en paramètre.
Exemple de données à envoyer (à passer dans le champ `params`) :
```
{
	logins: [
		"buchse_a",
		"schoch_h",
		"yazdi_r",
		"kiene_f"
	]
}
```
Exemple de données renvoyées :
```
[
  {
    "login": "buchse_a",
    "status": "at_home"
  },
  {
    "login": "schoch_h",
    "status": "at_school"
  },
  {
    "login": "kern_m",
    "status": "at_home"
  },
  {
    "login": "vermot_r",
    "status": "offline"
  }
]
```
* `GET /status/{login}` : Renvoie le status (`at_school`, `at_home` ou `offline`) de l'AER `login`. Exemple de données renvoyées :
```
[
	"at_school"
]
```
