<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IntraConnector;

class AERController extends Controller
{

	function permamence()
	{
		$url = 'http://milkyway.strasbourg.epitech.eu/redmine/projects/garde/issues.atom?key=4e7668f05a65553f5ed9b19661924ef64f294183&query_id=4';
		$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
		$xml = file_get_contents($url, false, $context);
		$xml = simplexml_load_string($xml);

		$logins = [];
		foreach ($xml->entry as $entry)
		{
			$login = $entry->title;
			$login = trim(preg_replace('#.+: ([a-z]+_[a-z])#', '$1', $login));
			$logins[] = $login;
		}

		return ($logins);
	}

	function permamenceCheck($match)
	{
		$url = 'http://milkyway.strasbourg.epitech.eu/redmine/projects/garde/issues.atom?key=4e7668f05a65553f5ed9b19661924ef64f294183&query_id=4';
		$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
		$xml = file_get_contents($url, false, $context);
		$xml = simplexml_load_string($xml);

		$logins = [];
		foreach ($xml->entry as $entry)
		{
			$login = $entry->title;
			$login = trim(preg_replace('#.+: ([a-z]+_[a-z])#', '$1', $login));
			$logins[] = $login;
		}

		return ([in_array($match, $logins)]);
	}

	function status(Request $request)
	{
		$params = $request->input('params');
		// Real datas or test datas...
		$params = !empty($params) ? json_decode($params) : (object)[
			'logins' => [
				'buchse_a',
				'schoch_h',
				'yazdi_r',
				'kiene_f',
			],
		];

		$intraConnector = new IntraConnector(env('INTRA_USER'), base64_decode(env('INTRA_PASSWORD')));

		$statusList = [];
		foreach ($params->logins as $login)
		{
			$profile = $intraConnector->getDatas("https://intra.epitech.eu/user/" . $login . "/?format=json");
			$profile = json_decode(preg_replace('#// Epitech JSON webservice ...#', '', $profile['response']));

			$status = "offline";
			if (!empty($profile->locations))
			{
				foreach ($profile->locations as $location)
					$status = $status != "at_school" && $location->pie ? "at_school" : "at_home";
			}
			$statusList[] = ['login' => $login, 'status' => $status];
		}

		return ($statusList);
	}

	function statusCheck($login)
	{
		$intraConnector = new IntraConnector(env('INTRA_USER'), base64_decode(env('INTRA_PASSWORD')));

		$profile = $intraConnector->getDatas("https://intra.epitech.eu/user/" . $login . "/?format=json");
		$profile = json_decode(preg_replace('#// Epitech JSON webservice ...#', '', $profile['response']));

		$status = "offline";
		if (!empty($profile->locations))
		{
			foreach ($profile->locations as $location)
				$status = $status != "at_school" && $location->pie ? "at_school" : "at_home";
		}

		return ([$status]);
	}

}
