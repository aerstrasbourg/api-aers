<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/permanence', ['as' => 'aer.permamence', 'uses' => 'AERController@permamence']);
$app->get('/permanence/{login: [a-z]+_[a-z]}', ['as' => 'aer.permamenceCheck', 'uses' => 'AERController@permamenceCheck']);

$app->post('/status', ['as' => 'aer.status', 'uses' => 'AERController@status']);
$app->get('/status/{login: [a-z]+_[a-z]}', ['as' => 'aer.statusCheck', 'uses' => 'AERController@statusCheck']);
